import smtplib, ssl
import datetime
import gmail
import bbdd
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

#https://medium.com/lyfepedia/sending-emails-with-gmail-api-and-python-49474e32c81f

def generaEmail(userHash, email):
    sender_email = "no_reply@4tic.com"
    receiver_email = email

    now = datetime.datetime.now()

    link = "http://fichador.4tic.com/"
    text = "¿Has fichado el fin de jornada? Hemos visto que registraste el inicio pero no el fin y pensamos que se te ha podido olvidar y te enviamos este mail como recordatorio. Puedes revisarlo en el siguiente enlace " + link
            
    message = MIMEMultipart("alternative")
    message["Subject"] = "¿Se te olvidó fichar el fin de jornada de hoy?"
    message["From"] = sender_email
    message["To"] = receiver_email
    message.attach(MIMEText(text, "plain"))
    return {'raw': base64.urlsafe_b64encode(message.as_string().encode('utf-8')).decode('ascii')}
    

def get_usuarios_con_fichajes_no_cerrados_hoy():
    records = []
    try:
        conn = bbdd.get_mysql_connection()
        sql_select_Query = "select hash, mail from v_usuarios_con_fichaje_no_cerrado_hoy";
        cursor = conn.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        cursor.close()
    except Error as e:
        print(e)
        records = []
 
    finally:
        if(conn is not None and conn.is_connected()):
            conn.close()
        return records
 
 
if __name__ == '__main__':
    records = get_usuarios_con_fichajes_no_cerrados_hoy()
    if len(records) > 0:
        for row in records:
            message = generaEmail(row[0], row[1])
            if (message is not None):
                gmail.send_message(message)
