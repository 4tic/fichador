import smtplib, ssl
import datetime
import gmail
import bbdd
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

#https://medium.com/lyfepedia/sending-emails-with-gmail-api-and-python-49474e32c81f

def generaEmail(userHash, email):
    sender_email = "no_reply@4tic.com"
    receiver_email = email

    now = datetime.datetime.now()

    link = "http://fichador.4tic.com/mishoraspdf.php?username=" + userHash + "&fecha=" + str(now.year) + "-" + '{:02d}'.format(now.month)
    text = "Ya tienes disponible el informe mensual de horas realizadas. Puedes descargarlo de la siguiente página " + link
    html = None

    try:
        with open('tpl-report-mensual-horas.html', encoding='utf8') as f:
            html = f.read()
    except Error as e:
        print(e)
        html = None

    if html is not None:
        html = html.replace('#URLINFORMEMENSUAL#', link)
        
        message = MIMEMultipart("alternative")
        message["Subject"] = "Informe mensual de horas realizadas"
        message["From"] = sender_email
        message["To"] = receiver_email
        message.attach(MIMEText(text, "plain"))
        message.attach(MIMEText(html, "html"))
        return {'raw': base64.urlsafe_b64encode(message.as_string().encode('utf-8')).decode('ascii')}
    else:
        return None

def get_usuarios_a_enviar_informe_mensual():
    records = []
    try:
        conn = bbdd.get_mysql_connection()
        sql_select_Query = "select hash, mail from v_usuarios_a_enviar_informe_mensual";
        cursor = conn.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        cursor.close()
    except Error as e:
        print(e)
        records = []
 
    finally:
        if(conn is not None and conn.is_connected()):
            conn.close()
        return records
 
 
if __name__ == '__main__':
    records = get_usuarios_a_enviar_informe_mensual()
    if len(records) > 0:
        for row in records:
            message = generaEmail(row[0], row[1])
            if (message is not None):
                gmail.send_message(message)
