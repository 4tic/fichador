from __future__ import print_function
from googleapiclient.discovery import build
from apiclient import errors
from httplib2 import Http
from email.mime.text import MIMEText
import base64
from google.oauth2 import service_account

def service_account_login():
	SCOPES = ['https://www.googleapis.com/auth/gmail.send']
	SERVICE_ACCOUNT_FILE = '/etc/4tic/fichador/service-key.json'
	
	credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
	delegated_credentials = credentials.with_subject("no_reply@4tic.com")
	service = build('gmail', 'v1', credentials=delegated_credentials)
	
	return service

def send_message(message):
	service = service_account_login()
	try:
		message = (service.users().messages().send(userId='me', body=message).execute())
		print('Message Id: %s' % message['id'])
		return message
	except errors.HttpError as error:
		print('An error occurred: %s' % error)