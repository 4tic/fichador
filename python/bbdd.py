import mysql.connector
from configparser import RawConfigParser

def get_mysql_connection():
	conn = None
	try:
		with open('/etc/4tic/fichador/app.properties') as f:
			file_content = '[dummy_section]\n' + f.read()
		parser = RawConfigParser()
		parser.read_string(file_content)
		config = parser['dummy_section']
		conn = mysql.connector.connect(host=config['host'],
									   database=config['db'],
									   user=config['user'],
									   password=config['pass'],
									   use_pure=True,
									   charset='utf8')
	except Error as e:
		print(e)
		conn = None
	
	finally:
		return conn