<?php
	include 'common.php';
    require 'lib/fpdf/fpdf.php';
	
    if (_isAllSet($_GET)) {
        $conexion = _get_conexion($server, $user, $pass, $bd);

        if (mysqli_connect_errno()) {
            error_log("Fall� la conexi�n: ". mysqli_connect_error());
            $response_array['status'] = 500;
            return $response_array;
        }

        $response_array = _get_horas_pdf($_GET, $conexion);
        $nombre_persona = _get_nombre_persona($_GET["username"], $conexion);
        $dni_persona = _get_dni_persona($_GET["username"], $conexion);

        $conexion->close();

        if ($response_array['status'] == 200) {
            $anyoMesArr = explode("-", $_GET['fecha']);
            $anyo = $anyoMesArr[0];
            $mes = $anyoMesArr[1];

            $pdf = new FPDF('L','mm','A4');
            $pdf->AddPage();
            _cabecezaDocumento($pdf, $nombre_persona, $anyo, $mes, $dni_persona);
            _cabeceraTabla($pdf);
            _datosTabla($pdf, $response_array["rows"]);
            //fdo empresa - fdo trabajador
            $pdf->Output();
        } else {
            http_response_code($response_array['status']);
        }
    } else {
        http_response_code(502);
    }

    function _get_dni_persona($username, $conexion) {
        $sql = "select dni from usuarios where hash = ?";
        $query = $conexion->prepare($sql);
        $query->bind_param("s", $username);
        $query->execute();
        $query->store_result();
        $query->bind_result($dni_persona);

        if ($query->fetch())
            return $dni_persona;
        else
            return '';
    }

    function _get_nombre_persona($username, $conexion) {
        $sql = "select nombre from usuarios where hash = ?";
        $query = $conexion->prepare($sql);
        $query->bind_param("s", $username);
        $query->execute();
        $query->store_result();
        $query->bind_result($nombre_persona);

        if ($query->fetch())
            return utf8_decode($nombre_persona);
        else
            return '';
    }

    function _get_horas_usuario_para_pdf($user, $anyo, $mes, $conexion) {
        $sql = "select date_format(fentrada.fecha, '%Y-%m-%d') dia, date_format(fentrada.fecha, '%H:%i') entrada, date_format(fsalida.fecha, '%H:%i') salida, fentrada.observaciones ";
        $sql .= "from v_fichajes_validos fentrada, v_fichajes_validos fsalida ";
        $sql .= "where fsalida.hash = ? and fsalida.hash = fentrada.hash ";
        $sql .= "and date_format(fentrada.fecha, '%Y') = ? and date_format(fsalida.fecha, '%Y') = ? ";
        $sql .= "and date_format(fentrada.fecha, '%m') = ? and date_format(fsalida.fecha, '%m') = ? ";
        $sql .= "and date_format(fentrada.fecha, '%Y-%m-%d') = date_format(fsalida.fecha, '%Y-%m-%d') and fentrada.tipo = 'entrada' and fsalida.tipo = 'salida' ";
        $sql .= "and fentrada.fecha < fsalida.fecha ";
        $sql .= "and fsalida.id = (";
        $sql .= "select id from v_fichajes_validos where tipo = 'salida' and hash = ? and fecha > fentrada.fecha order by fecha asc limit 1";
        $sql .= ")  order by fentrada.fecha desc, fsalida.fecha desc;";
        $query = $conexion->prepare($sql);
        $query->bind_param("ssssss", $user, $anyo, $anyo, $mes, $mes, $user);
        $query->execute();
        $query->store_result();
        $query->bind_result($dia, $entrada, $salida, $observaciones);
        $resultado = array();

        $entra = false;
        $i = 0;

        while ($query->fetch()) {
            $entra = true;
            $resultado[$i] = array(
                'dia' => $dia,
                'horaSalida' => $salida,
                'horaEntrada' => $entrada,
                'observaciones' => $observaciones
            );
            $i = $i+1;
        }
        $query->close();
        return $resultado;
    }

    function _get_horas_pdf($post, $conexion) {
        $username = $post['username'];
        $anyoMesArr = explode("-", $post['fecha']);
        $anyo = $anyoMesArr[0];
        $mes = $anyoMesArr[1];

        if (_existe_usuario($username, $conexion)) {
            $horas_pdf = _get_horas_usuario_para_pdf($username, $anyo, $mes, $conexion);
            $response_array['status'] = 200;
            $response_array['rows'] = $horas_pdf;
        } else {
            $response_array['status'] = 403;
        }
        return $response_array;
    }

    function _negrita($pdf, $text, $width) {
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell($width, 10, $text);
    }

    function _plain($pdf, $text, $width, $border, $align, $ln) {
        if (!isset($width))
            $width = 0;

        if (!isset($border))
            $border = 0;

        if (!isset($ln))
            $ln = 0;

        if (!isset($align))
            $align = 'L';

        $pdf->SetFont('Arial','',10);
        $pdf->Cell($width, 10, $text, $border, $ln, $align);
    }

    function _cabecezaDocumento($pdf, $username, $anyo, $mes, $dni) {
        $months = array (1=>'Enero',2=>'Febrero',3=>'Marzo',4=>'Abril',5=>'Mayo',6=>'Junio',7=>'Julio',8=>'Agosto',9=>'Septiembre',10=>'Octubre',11=>'Noviembre',12=>'Diciembre');
        $monthName = $months[(int)$mes];

        $pdf->SetFont('Arial','',16);
        $pdf->Cell(120,10,'REGISTRO DIARIO DE JORNADAS');
        _plain($pdf, 'En cumplimiento de la obligaci�n establecida en el art. 12.4 C) del Estatuto de los Trabajadores', 80, null, null, null);
        $pdf->Ln();
        _negrita($pdf, 'Empresa: ', 20);
        _plain($pdf, '4TIC CASTELL�N 2009 S.L.', 70, null, null, null);
        _negrita($pdf, 'CIF: ', 10);
        _plain($pdf, 'B-12817748', 40, null, null, null);
        _negrita($pdf, 'CCC: ', 10);
        _plain($pdf, '12107790580', 40, null, null, null);
        _negrita($pdf, 'Mes: ', 20);
        _plain($pdf, $monthName, 70, null, null, null);
        $pdf->Ln();
        _negrita($pdf, 'Trabajador: ', 20);
        _plain($pdf, $username, 70, null, null, null);
        _negrita($pdf, 'NIF: ', 10);
        _plain($pdf, $dni, 40, null, null, null);
        _negrita($pdf, 'NAF: ', 10);
        _plain($pdf, '', 40, null, null, null);
        _negrita($pdf, 'A�o: ', 20);
        _plain($pdf, $anyo, 70, null, null, null);
        $pdf->Ln();
    }

    function _cabeceraTabla($pdf) {
        _plain($pdf, 'Fecha', 40, 1, 'C', null);
        _plain($pdf, 'Hora inicio', 40, 1, 'C', null);
        _plain($pdf, 'Hora fin', 40, 1, 'C', null);
        _plain($pdf, 'Total de horas', 40, 1, 'C', null);
        _plain($pdf, 'Observaciones', 0, 1, 'C', null);
        $pdf->Ln();
    }

    function _restaFechas($horaFinal, $horaInicial) {
        if (isset($horaFinal) && isset($horaInicial)) {
            $dtDiff = date_diff(date_create('2019-01-01 '.$horaFinal), date_create('2019-01-01 '.$horaInicial));
            return $dtDiff->format("%H:%I");
        }
        return '';
    }

    function _datosTabla($pdf, $fichajes) {
        $horasTrabajadas = array();

        for ($i=0;$i<count($fichajes);$i++) {
            _plain($pdf, $fichajes[$i]["dia"], 40, 1, 'C', null);
            _plain($pdf, $fichajes[$i]["horaEntrada"], 40, 1, 'C', null);
            _plain($pdf, $fichajes[$i]["horaSalida"], 40, 1, 'C', null);
            $horasDia = _restaFechas($fichajes[$i]["horaSalida"], $fichajes[$i]["horaEntrada"]);
            _plain($pdf, $horasDia, 40, 1, 'C', null);
            _plain($pdf, $fichajes[$i]["observaciones"], 0, 1, 'C', null);
            $pdf->Ln();
            $horasTrabajadas[] = $horasDia;
        }

        _horasTrabajadas($pdf, $horasTrabajadas);
    }

    function _horasTrabajadas($pdf, $horasTrabajadas) {
        $minutes = 0;
        
        foreach ($horasTrabajadas as $horasTrabajadasDia) {
            list($hour, $minute) = explode(':', $horasTrabajadasDia);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

        _negrita($pdf, 'Horas trabajadas durante este mes: '.sprintf('%02d horas %02d minutos', $hours, $minutes), '40');
    }
?>