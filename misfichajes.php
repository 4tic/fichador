<?php
	include 'common.php';
	header('Content-type: application/json');
	date_default_timezone_set("Europe/Berlin");

	function borrar_fichaje($id, $post, $conexion) {
		if (_isAllSet($post) && isset($id)) {
			$username = $post['username'];
			$sql = "select (id = ?) as borrable from v_fichajes_validos where hash = ? order by fecha desc, id desc limit 1";
			$query = $conexion->prepare($sql);
			$query->bind_param("is", $id, $username);
			$query->execute();
			$query->store_result();
			$query->bind_result($borrable); 
			$resultado = array();

			$query->fetch();
			if ($borrable) {
				error_log("borramos id".$id.'--'.$post["observaciones_borrado"]);
				$sql = "update fichajes set borrado = current_date, observaciones_borrado = ? where id = ?";
				$query = $conexion->prepare($sql);
				$query->bind_param("si", $post['observaciones_borrado'], $id);
				$query->execute();
			}
			$query->close();
		}
	}

    function _get_fichajes_usuario($user, $anyo, $mes, $conexion) {
    	$sql = "SELECT fecha, tipo, observaciones, id FROM v_fichajes_validos where hash = ? ";
    	$sql .= "and date_format(fecha, '%Y') = ? and date_format(fecha, '%m') = ? ";
    	$sql .= "order by fecha desc";
    	$query = $conexion->prepare($sql);
		$query->bind_param("sss", $user, $anyo, $mes);
		$query->execute();
		$query->store_result();
		$query->bind_result($fecha, $tipo, $observaciones, $id); 
		$resultado = array();

		$borrableSeleccionado = false;

		while ($query->fetch()) {
			if ($borrableSeleccionado)
				$linkBorrar = '';
			else {
				$linkBorrar = '<a onclick="javascript:borrar('.$id.')"><i class="borrar fas fa-trash"></i></a>';
				$borrableSeleccionado = true;
			}
        	$resultado[] = array(
				'fecha' => $fecha,
				'tipo' => $tipo,
				'borrar' => $linkBorrar
			);
    	}

    	$query->close();
    	return $resultado;
    }

    function get_mis_fichajes($post, $conexion) {
    	if (_isAllSet($post)) {
			$username = $post['username'];
			$anyoMesArr = explode("-", $post['fecha']);
			$anyo = $anyoMesArr[0];
			$mes = $anyoMesArr[1];

			if (_existe_usuario($username, $conexion)) {
				$fichajes = _get_fichajes_usuario($username, $anyo, $mes, $conexion);
				$response_array['status'] = 200;
				$response_array['current'] = 1;
				$response_array['rowCount'] = count($fichajes);
				$response_array['rows'] = $fichajes;
	    	} else {
	    		$response_array['status'] = 403;
	    	}
	    } else
	    	$response_array['status'] = 502;

	    return $response_array;
    }

    $conexion = _get_conexion($server, $user, $pass, $bd);

    if (mysqli_connect_errno()) {
        error_log("Falló la conexión: ". mysqli_connect_error());
        $response_array['status'] = 500;
        return $response_array;
    }

    if (isset($_GET["id"])) {
    	borrar_fichaje($_GET["id"], $_POST, $conexion);
    }

    $response_array = get_mis_fichajes($_POST, $conexion);
    $conexion->close();

    if ($response_array['status'] > 400) {
    	die(header("HTTP/1.0 " . $response_array['status']));
    } else {
		echo json_encode($response_array);
	}
?>