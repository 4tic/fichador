<?php
	include 'config.php';

	function _isAllSet($var) {
    	return isset($var['username']) && isset($var['fecha']) && _isFechaValida($var["fecha"]);
    }

    //formato yyyy-mm
    function _isFechaValida($fecha) {
    	return preg_match("/^(20[1-2][0-9])-(0[1-9]|1[0-2])$/", $fecha);
    }

	function _existe_usuario($user, $conexion) {
    	$cantidad = 0;
		$query = $conexion->prepare("SELECT count(*) FROM usuarios where hash = ? and fecha_baja is null");
		$query->bind_param("s", $user);
		$query->execute();
		$query->store_result();
		$query->bind_result($cantidad); 
		$query->fetch();

        $existe = ($cantidad > 0);
        $query->close();

		if (!$existe)
			error_log("NO existe el usuario " . $user);
		return $existe;
    }

    function _get_horas_usuario($user, $anyo, $mes, $conexion) {
    	$sql = "select date_format(fentrada.fecha, '%Y-%m-%d') dia, time_format(timediff(fsalida.fecha, fentrada.fecha),'%H:%i') as horas ";
    	$sql .= "from v_fichajes_validos fentrada, v_fichajes_validos fsalida ";
    	$sql .= "where fsalida.hash = ? and fsalida.hash = fentrada.hash ";
    	$sql .= "and date_format(fentrada.fecha, '%Y') = ? and date_format(fsalida.fecha, '%Y') = ? ";
    	$sql .= "and date_format(fentrada.fecha, '%m') = ? and date_format(fsalida.fecha, '%m') = ? ";
    	$sql .= "and date_format(fentrada.fecha, '%Y-%m-%d') = date_format(fsalida.fecha, '%Y-%m-%d') and fentrada.tipo = 'entrada' and fsalida.tipo = 'salida' ";
    	$sql .= "and fentrada.fecha < fsalida.fecha and fsalida.id = ";
    	$sql .= "(select id from v_fichajes_validos where tipo = 'salida' and hash = ? and fecha > fentrada.fecha order by fecha asc limit 1) ";
    	$sql .= "order by fentrada.fecha desc, fsalida.fecha desc";
    	$query = $conexion->prepare($sql);
		$query->bind_param("ssssss", $user, $anyo, $anyo, $mes, $mes, $user);
		$query->execute();
		$query->store_result();
		$query->bind_result($dia, $horas);
		$resultado = array();

		$diaAnterior = '';
		$horasSumadas = "00:00";
		$entra = false;

		while ($query->fetch()) {
			$entra = true;
			if ($dia != $diaAnterior) {
				$resultado[($diaAnterior == '')? $dia : $diaAnterior] = $horasSumadas;
				$horasSumadas = "00:00";
			}

			if ($diaAnterior == '')
				$diaAnterior = $dia;

			$horasSumadas = _suma_horas($horasSumadas, $horas);
			$diaAnterior = $dia;
    	}
    	$query->close();
    	$resultadoFechas = array();
    	
    	if ($entra) {
    		$resultado[$diaAnterior] = $horasSumadas;
    		
    		$i = 0;

    		while (list($key, $value) = each($resultado)) {
    			$resultadoFechas[$i] = array(
    				'fecha' => $key,
    				'horas' => $value
    			);
    			$i = $i+1;
    		}
    	}
    	return $resultadoFechas;
    }

    function _suma_horas($horas, $horasASumar) {
    	$times = array();
    	$times[] = $horas;
    	$times[] = $horasASumar;
    	$minutes = 0;

    	foreach ($times as $time) {
    		list($hour, $minute) = explode(':', $time);
    		$minutes += $hour * 60;
        	$minutes += $minute;
    	}
    	$hours = floor($minutes / 60);
    	$minutes -= $hours * 60;

    	return sprintf('%02d:%02d', $hours, $minutes);
    }

    function get_mis_horas($post, $conexion) {
		$username = $post['username'];
		$anyoMesArr = explode("-", $post['fecha']);
        $anyo = $anyoMesArr[0];
		$mes = $anyoMesArr[1];

		if (_existe_usuario($username, $conexion)) {
			$horas = _get_horas_usuario($username, $anyo, $mes, $conexion);
			$response_array['status'] = 200;
			$response_array['current'] = 1;
			$response_array['rowCount'] = count($horas);
			$response_array['rows'] = $horas;
    	} else {
    		$response_array['status'] = 403;
    	}
    	$conexion->close();
	    return $response_array;
    }

    function _get_conexion($server, $user, $pass, $bd) {
        return new mysqli($server, $user, $pass, $bd);
    }
?>