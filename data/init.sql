CREATE TABLE `usuarios` (
  `hash` varchar(7) NOT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_baja` datetime DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `dni` varchar(15) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `fichajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(15) NOT NULL,
  `fecha` timestamp NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `observaciones` text,
  `fecha_introduccion_fichaje` timestamp default CURRENT_TIMESTAMP,
  `borrado` datetime default null,
  `observaciones_borrado` text,
  PRIMARY KEY (`id`),
  KEY `fichajes_ibfk_1` (`hash`),
  CONSTRAINT `fichajes_ibfk_1` FOREIGN KEY (`hash`) REFERENCES `usuarios` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create view v_fichajes_validos as select * from fichajes where borrado is null;
create view v_usuarios_a_enviar_informe_mensual as select * from usuarios where fecha_baja is null;
create view v_usuarios_con_fichaje_no_cerrado_hoy as 
  select distinct u.hash, u.mail 
  from fichajes fentrada, usuarios u 
  where u.hash = fentrada.hash 
  and u.fecha_baja is null 
  and date_format(fentrada.fecha, '%Y-%m-%d') = current_date()
  and fentrada.borrado is null 
  and fentrada.tipo = 'entrada' 
  and (
    select count(*) from fichajes fsalida where fsalida.tipo = 'salida' and date_format(fsalida.fecha, '%Y-%m-%d') = current_date() and fsalida.borrado is null and fsalida.hash = u.hash
    ) = 0;

CREATE TABLE `vacaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(15) NOT NULL,
  `fecha` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fichajes_vac_1` (`hash`),
  CONSTRAINT `fichajes_vac_1` FOREIGN KEY (`hash`) REFERENCES `usuarios` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create view v_usuarios_sin_fichaje_hoy as 
  select distinct u.hash, u.mail 
  from usuarios u 
  where u.fecha_baja is null 
  and (
    select count(*) from fichajes where hash = u.hash and borrado is null and date_format(fecha, '%Y-%m-%d') = current_date()
    ) = 0 
  and (
    select count(*) from vacaciones where hash = u.hash and date_format(fecha,'%Y-%m-%d') = current_date()
    ) = 0
  and (
    select count(*) from fiestas where date_format(fecha,'%Y-%m-%d') = current_date()
    ) = 0;

CREATE TABLE `fiestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- fiestas Castellón 2019
insert into fiestas (fecha) values ('2019-01-01');
insert into fiestas (fecha) values ('2019-03-19');
insert into fiestas (fecha) values ('2019-03-25');
insert into fiestas (fecha) values ('2019-04-19');
insert into fiestas (fecha) values ('2019-04-22');
insert into fiestas (fecha) values ('2019-05-01');
insert into fiestas (fecha) values ('2019-06-24');
insert into fiestas (fecha) values ('2019-06-29');
insert into fiestas (fecha) values ('2019-08-15');
insert into fiestas (fecha) values ('2019-10-09');
insert into fiestas (fecha) values ('2019-10-12');
insert into fiestas (fecha) values ('2019-11-01');
insert into fiestas (fecha) values ('2019-12-06');
insert into fiestas (fecha) values ('2019-12-25');
--