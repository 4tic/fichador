<?php
    include 'common.php';
	header('Content-type: application/json');
	date_default_timezone_set("Europe/Berlin");

    $conexion = _get_conexion($server, $user, $pass, $bd);

    if (mysqli_connect_errno()) {
        error_log("Falló la conexión: ". mysqli_connect_error());
        $response_array['status'] = 500;
        return $response_array;
    }
    
    if (_isAllSet($_POST)) {
        $response_array = get_mis_horas($_POST, $conexion);
    } else
        $response_array['status'] = 502;

    $conexion->close();

    if ($response_array['status'] > 400) {
    	die(header("HTTP/1.0 " . $response_array['status']));
    } else {
		echo json_encode($response_array);
	}
?>