<?php
	include 'config.php';
	include 'common.php';
	header('Content-type: application/json');
	date_default_timezone_set("Europe/Berlin");

	function _isAllSetBeforeFichar($var) {
    	$areSet =  isset($var['txtDate']) && isset($var['txtHora']) && isset($var['username']) && isset($var['tipo']);
    	$dateValid = (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$var['txtDate']));

    	return $areSet && $dateValid;
    }

    function _get_cantidad_fichajes_usuario($user, $conexion) {
		$query = $conexion->prepare("SELECT count(*) FROM v_fichajes_validos where hash = ?");
		$query->bind_param("s", $user);
		$query->execute();
		$query->store_result();
		$query->bind_result($cantidad); 
		$query->fetch();
		$query->close();
		return $cantidad;
    }

    function _existe_fichaje_mismo_tipo_mismo_dia($user, $tipo, $fecha, $fechaHora, $conexion) {
    	$cantidad = 0;
    	$dayOfYear = date("z", strtotime($fecha))+1;
		
		$query = $conexion->prepare("SELECT count(*) FROM v_fichajes_validos where hash = ? and date_format(fecha, '%j') = ? and tipo = ?");
		$query->bind_param("sis", $user, $dayOfYear, $tipo);
		$query->execute();
		$query->store_result();
		$query->bind_result($cantidad); 
		$query->fetch();

        $existe = ($cantidad > 0);
        $query->close();

		if ($existe) {
			$query = $conexion->prepare("SELECT tipo FROM v_fichajes_validos where hash = ? and date_format(fecha, '%j') = ? and fecha <= ? order by fecha desc, id desc limit 1");
			$query->bind_param("sis", $user, $dayOfYear, $fechaHora);
			$query->execute();
			$query->store_result();
			$query->bind_result($ultimoTipoInsertadoEnFecha); 
			$query->fetch();

        	$existe = ($ultimoTipoInsertadoEnFecha == $tipo);
        	$query->close();
        	if ($existe)
				error_log("Ya hay un fichaje para el dia " . $fecha . ' el usuario ' . $user .' y del tipo ' .$tipo);
		}
		return $existe;
    }

    function _es_ultimo_fichaje_mismo_tipo($username, $tipo, $fechaHora, $conexion) {
    	$query = $conexion->prepare("SELECT tipo FROM v_fichajes_validos where hash = ? and fecha <= ? order by fecha desc limit 1");
		$query->bind_param("ss", $username, $fechaHora);
		$query->execute();
		$query->store_result();
		$query->bind_result($ultimoTipo); 
		$query->fetch();

		$query->close();
		return ($ultimoTipo == $tipo);
    }

    function procesa_fichaje($post, $conexion) {
    	if (_isAllSetBeforeFichar($post)) {
			$txtFechaHora = $post['txtDate'].' '.$post['txtHora'];
			$username = $post['username'];
			$tipo = $post['tipo'];
			$observaciones = $post['observaciones'];

			if (_existe_usuario($username, $conexion)) {
				$cantidadFichajes = _get_cantidad_fichajes_usuario($username, $conexion);
				
				if ($cantidadFichajes == 0 || !_existe_fichaje_mismo_tipo_mismo_dia($username, $tipo, $post['txtDate'], $txtFechaHora, $conexion)) {
					if (!_es_ultimo_fichaje_mismo_tipo($username, $tipo, $txtFechaHora, $conexion)) {
						$query = $conexion->prepare("INSERT INTO fichajes (hash, fecha, tipo, observaciones) VALUES (?, ?, ?, ?)");
						$query->bind_param("ssss", $username, $txtFechaHora, $tipo, $observaciones);
						$query->execute();
						$query->close();

						//BLOCKCHAIN
						$txtFechaBC = urlencode($txtFechaHora);
						$urlBC = 'http://116.203.245.175:8086/rest/horas/'.$username.'?type='.$tipo.'&date='.$txtFechaBC.'&token=fdasjkfhn4ucn4783nbfkjadh324fn9hc';
						$ch = curl_init(); 
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, $urlBC);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						$output = curl_exec($ch); 
						curl_close($ch);
						
						if ($conexion->error != '') {
							error_log($conexion->error);
							$response_array['status'] = 501;
						} else
			    			$response_array['status'] = 204;
		    		} else
		    			$response_array['status'] = 504;
		    	} else 
		    		$response_array['status'] = 503;
	    	} else {
	    		$response_array['status'] = 403;
	    	}
	    } else
	    	$response_array['status'] = 502;

	    return $response_array;
    }

    $conexion = _get_conexion($server, $user, $pass, $bd);

    if (mysqli_connect_errno()) {
        error_log("Falló la conexión: ". mysqli_connect_error());
        $response_array['status'] = 500;
        return $response_array;
    }

    $response_array = procesa_fichaje($_POST, $conexion);
    $conexion->close();

    if ($response_array['status'] > 400) {
    	die(header("HTTP/1.0 " . $response_array['status']));
    } else {
		echo json_encode($response_array);
	}
?>