# Ejecución de los diferentes scripts

Es necesaria de la existencia de un archivo .credential al nivel de entrada.sh y salida.sh con el contenido del usuario:

USER=xtestx

Se debe colocar los scripts en /etc/init.d y enlazarlos en los runlevels de apagado y encendido:

## APAGADO
/etc/rc0.d/K99salida -> /etc/init.d/salida.sh

## ENCENDIDO
/etc/rc1.d/K20entrada -> /etc/init.d/entrada.sh
/etc/rc2.d/K20entrada -> /etc/init.d/entrada.sh
/etc/rc3.d/K20entrada -> /etc/init.d/entrada.sh
/etc/rc4.d/K20entrada -> /etc/init.d/entrada.sh
/etc/rc5.d/K20entrada -> /etc/init.d/entrada.sh
