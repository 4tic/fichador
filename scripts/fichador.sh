#!/bin/bash
### BEGIN INIT INFO
# Provides:          fichador
# Required-Start:    $network $syslog
# Required-Stop:     $network $syslog
# Default-Start:     5
# Default-Stop:      0
# Short-Description: script de fichaje 4TIC
### END INIT INFO

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

. $DIR/.credential
. $DIR/.date

if [ "$1" = "stop" ]
then
    logger "adiós"
    curl -vvvv 'http://fichador.4tic.com/store.php' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' --data "txtDate=${DATE}&txtHora=${HOUR}&username=${USER}&tipo=salida&observaciones="
elif [ "$1" = "start" ]
then
    logger "hola"
    curl -vvvv 'http://fichador.4tic.com/store.php' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' --data "txtDate=${DATE}&txtHora=${HOUR}&username=${USER}&tipo=entrada&observaciones="
fi
